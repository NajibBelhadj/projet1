<?php

namespace App\Repository;

use App\Entity\RefHotel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RefHotel|null find($id, $lockMode = null, $lockVersion = null)
 * @method RefHotel|null findOneBy(array $criteria, array $orderBy = null)
 * @method RefHotel[]    findAll()
 * @method RefHotel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RefHotelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RefHotel::class);
    }

    // /**
    //  * @return RefHotel[] Returns an array of RefHotel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RefHotel
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
