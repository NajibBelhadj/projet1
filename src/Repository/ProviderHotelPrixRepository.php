<?php

namespace App\Repository;

use App\Entity\ProviderHotelPrix;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ProviderHotelPrix|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProviderHotelPrix|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProviderHotelPrix[]    findAll()
 * @method ProviderHotelPrix[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProviderHotelPrixRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProviderHotelPrix::class);
    }

    // /**
    //  * @return ProviderHotelPrix[] Returns an array of ProviderHotelPrix objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProviderHotelPrix
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
