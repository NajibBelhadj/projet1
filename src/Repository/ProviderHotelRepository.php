<?php

namespace App\Repository;

use App\Entity\ProviderHotel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ProviderHotel|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProviderHotel|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProviderHotel[]    findAll()
 * @method ProviderHotel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProviderHotelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProviderHotel::class);
    }

    // /**
    //  * @return ProviderHotel[] Returns an array of ProviderHotel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProviderHotel
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
