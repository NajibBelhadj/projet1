<?php

namespace App\DataFixtures;

use App\Entity\Provider;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ProviderFixures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        $provider1 = new Provider();
        $provider1->setName('Tunisiebooking')
                   ->setDescription("TunisieBooking.com est une marque exclusive de Spring Travel Services, agence de voyage catégorie A sous licence n° 8517.                   Tunisie Booking est une société de commercialisation en ligne dédié à l'hôtellerie en Tunisie (hôtels, villas, appartement), proposant aussi bien des prix professionnels pour les agences de voyages ainsi que des services annexes consacrés aux voyages tels que la location de voitures, les tracks.")
                   ->setLink("https://tn.tunisiebooking.com/");
        $provider2 = new Provider();
        $provider2->setName('Tahavoyage')
                    ->setDescription("Taha Voyages, la meilleure agence de voyages en Tunisie, offrant des voyages organisés Istanbul, maroc, des séjours d'hôtels, des Billets d'avion et Omra")
                    ->setLink('https://www.tahavoyages.com/');
        $manager->persist($provider1);
        $manager->persist($provider2);
        $manager->flush();
    }
}
