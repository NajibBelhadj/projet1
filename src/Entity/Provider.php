<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProviderRepository")
 */
class Provider
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $link;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $provider_ref;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ProviderHotel", inversedBy="providers")
     */
    private $providerhotels;
    
    public function __construct()
    {
        $this->providerhotels = new ArrayCollection();
        $this->providerHotels = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getProviderRef(): ?bool
    {
        return $this->provider_ref;
    }

    public function setProviderRef(bool $provider_ref): self
    {
        $this->provider_ref = $provider_ref;

        return $this;
    }

    /**
     * @return Collection|ProviderHotel[]
     */
    public function getProviderhotels(): Collection
    {
        return $this->providerhotels;
    }

    public function addProviderhotel(ProviderHotel $providerhotel): self
    {
        if (!$this->providerhotels->contains($providerhotel)) {
            $this->providerhotels[] = $providerhotel;
        }

        return $this;
    }

    public function removeProviderhotel(ProviderHotel $providerhotel): self
    {
        if ($this->providerhotels->contains($providerhotel)) {
            $this->providerhotels->removeElement($providerhotel);
        }

        return $this;
    } 
    public function __toString()
    {
        return $this->name;
    }   

}
