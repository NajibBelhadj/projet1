<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RefHotelRepository")
 */
class RefHotel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ProviderHotel", cascade={"persist", "remove"})
     */
    private $ref_hotel_t;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ProviderHotel", cascade={"persist", "remove"})
     */
    private $ref_hotel_tb;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRefHotelT(): ?ProviderHotel
    {
        return $this->ref_hotel_t;
    }

    public function setRefHotelT(?ProviderHotel $ref_hotel_t): self
    {
        $this->ref_hotel_t = $ref_hotel_t;

        return $this;
    }

    public function getRefHotelTb(): ?ProviderHotel
    {
        return $this->ref_hotel_tb;
    }

    public function setRefHotelTb(?ProviderHotel $ref_hotel_tb): self
    {
        $this->ref_hotel_tb = $ref_hotel_tb;

        return $this;
    }
}
