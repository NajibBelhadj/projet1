<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProviderHotelRepository")
 */
class ProviderHotel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $link;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Provider", mappedBy="providerhotels")
     */
    private $providers;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hotel_ref;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProviderHotelPrix", mappedBy="providerhotels")
     */
    private $providerHotelPrixes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProviderHotelImg", mappedBy="providerhotels")
     */
    private $providerHotelImgs;


    public function __construct()
    {
        $this->providers = new ArrayCollection();
        $this->providerHotelImgs = new ArrayCollection();
        $this->providerHotelPrixes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    /**
     * @return Collection|Provider[]
     */
    public function getProviders(): Collection
    {
        return $this->providers;
    }

    public function addProvider(Provider $provider): self
    {
        if (!$this->providers->contains($provider)) {
            $this->providers[] = $provider;
            $provider->addProviderhotel($this);
        }

        return $this;
    }

    public function removeProvider(Provider $provider): self
    {
        if ($this->providers->contains($provider)) {
            $this->providers->removeElement($provider);
            $provider->removeProviderhotel($this);
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getHotelRef(): ?bool
    {
        return $this->hotel_ref;
    }

    public function setHotelRef(?bool $hotel_ref): self
    {
        $this->hotel_ref = $hotel_ref;

        return $this;
    }



    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return Collection|ProviderHotelPrix[]
     */
    public function getProviderHotelPrixes(): Collection
    {
        return $this->providerHotelPrixes;
    }

    public function addProviderHotelPrix(ProviderHotelPrix $providerHotelPrix): self
    {
        if (!$this->providerHotelPrixes->contains($providerHotelPrix)) {
            $this->providerHotelPrixes[] = $providerHotelPrix;
            $providerHotelPrix->setProviderhotels($this);
        }

        return $this;
    }

    public function removeProviderHotelPrix(ProviderHotelPrix $providerHotelPrix): self
    {
        if ($this->providerHotelPrixes->contains($providerHotelPrix)) {
            $this->providerHotelPrixes->removeElement($providerHotelPrix);
            // set the owning side to null (unless already changed)
            if ($providerHotelPrix->getProviderhotels() === $this) {
                $providerHotelPrix->setProviderhotels(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProviderHotelImg[]
     */
    public function getProviderHotelImgs(): Collection
    {
        return $this->providerHotelImgs;
    }

    public function addProviderHotelImg(ProviderHotelImg $providerHotelImg): self
    {
        if (!$this->providerHotelImgs->contains($providerHotelImg)) {
            $this->providerHotelImgs[] = $providerHotelImg;
            $providerHotelImg->setProviderhotels($this);
        }

        return $this;
    }

    public function removeProviderHotelImg(ProviderHotelImg $providerHotelImg): self
    {
        if ($this->providerHotelImgs->contains($providerHotelImg)) {
            $this->providerHotelImgs->removeElement($providerHotelImg);
            // set the owning side to null (unless already changed)
            if ($providerHotelImg->getProviderhotels() === $this) {
                $providerHotelImg->setProviderhotels(null);
            }
        }

        return $this;
    }
    

   
}
