<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200217233242 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE provider CHANGE provider_ref provider_ref TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE provider_hotel CHANGE hotel_ref hotel_ref TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE provider_hotel_img CHANGE providerhotelimgs_id providerhotelimgs_id INT DEFAULT NULL, CHANGE providerhotels_id providerhotels_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE provider_hotel_prix CHANGE providerhotels_id providerhotels_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE provider CHANGE provider_ref provider_ref TINYINT(1) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE provider_hotel CHANGE hotel_ref hotel_ref TINYINT(1) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE provider_hotel_img CHANGE providerhotelimgs_id providerhotelimgs_id INT DEFAULT NULL, CHANGE providerhotels_id providerhotels_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE provider_hotel_prix CHANGE providerhotels_id providerhotels_id INT DEFAULT NULL');
    }
}
