<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200217213610 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE provider_hotel_provider DROP FOREIGN KEY FK_88786E467A01D5F1');
        $this->addSql('CREATE TABLE providerhotel (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, link VARCHAR(255) NOT NULL, continu VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('DROP TABLE provider_hotel');
        $this->addSql('DROP TABLE provider_hotel_provider');
        $this->addSql('ALTER TABLE provider CHANGE provider_ref provider_ref TINYINT(1) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE provider_hotel (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, category_hotel VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, description LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, hotel_ref TINYINT(1) DEFAULT \'NULL\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE provider_hotel_provider (provider_hotel_id INT NOT NULL, provider_id INT NOT NULL, INDEX IDX_88786E46A53A8AA (provider_id), INDEX IDX_88786E467A01D5F1 (provider_hotel_id), PRIMARY KEY(provider_hotel_id, provider_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE provider_hotel_provider ADD CONSTRAINT FK_88786E467A01D5F1 FOREIGN KEY (provider_hotel_id) REFERENCES provider_hotel (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE provider_hotel_provider ADD CONSTRAINT FK_88786E46A53A8AA FOREIGN KEY (provider_id) REFERENCES provider (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE providerhotel');
        $this->addSql('ALTER TABLE provider CHANGE provider_ref provider_ref TINYINT(1) DEFAULT \'NULL\'');
    }
}
