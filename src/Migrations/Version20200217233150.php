<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200217233150 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE provider CHANGE provider_ref provider_ref TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE provider_hotel CHANGE hotel_ref hotel_ref TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE provider_hotel_img ADD providerhotels_id INT DEFAULT NULL, CHANGE providerhotelimgs_id providerhotelimgs_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE provider_hotel_img ADD CONSTRAINT FK_1AD6636092AD4D41 FOREIGN KEY (providerhotels_id) REFERENCES provider_hotel (id)');
        $this->addSql('CREATE INDEX IDX_1AD6636092AD4D41 ON provider_hotel_img (providerhotels_id)');
        $this->addSql('ALTER TABLE provider_hotel_prix CHANGE providerhotels_id providerhotels_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE provider CHANGE provider_ref provider_ref TINYINT(1) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE provider_hotel CHANGE hotel_ref hotel_ref TINYINT(1) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE provider_hotel_img DROP FOREIGN KEY FK_1AD6636092AD4D41');
        $this->addSql('DROP INDEX IDX_1AD6636092AD4D41 ON provider_hotel_img');
        $this->addSql('ALTER TABLE provider_hotel_img DROP providerhotels_id, CHANGE providerhotelimgs_id providerhotelimgs_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE provider_hotel_prix CHANGE providerhotels_id providerhotels_id INT DEFAULT NULL');
    }
}
