<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200217233644 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE provider CHANGE provider_ref provider_ref TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE provider_hotel CHANGE hotel_ref hotel_ref TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE provider_hotel_img DROP FOREIGN KEY FK_1AD6636024B7829');
        $this->addSql('DROP INDEX IDX_1AD6636024B7829 ON provider_hotel_img');
        $this->addSql('ALTER TABLE provider_hotel_img DROP providerhotelimgs_id, CHANGE providerhotels_id providerhotels_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE provider_hotel_prix CHANGE providerhotels_id providerhotels_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE provider CHANGE provider_ref provider_ref TINYINT(1) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE provider_hotel CHANGE hotel_ref hotel_ref TINYINT(1) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE provider_hotel_img ADD providerhotelimgs_id INT DEFAULT NULL, CHANGE providerhotels_id providerhotels_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE provider_hotel_img ADD CONSTRAINT FK_1AD6636024B7829 FOREIGN KEY (providerhotelimgs_id) REFERENCES provider_hotel (id)');
        $this->addSql('CREATE INDEX IDX_1AD6636024B7829 ON provider_hotel_img (providerhotelimgs_id)');
        $this->addSql('ALTER TABLE provider_hotel_prix CHANGE providerhotels_id providerhotels_id INT DEFAULT NULL');
    }
}
