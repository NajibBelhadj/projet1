<?php

namespace App\Controller;

use App\Entity\Provider;
use App\Entity\ProviderHotel;
use App\Entity\ProviderHotelImg;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HotelBookingController extends AbstractController
{
    /**
     * @Route("/hotel/booking", name="hotel_booking")
     */
    public function index()
    {
        for($i = 6; $i < 615; $i++)
        {   
            $entityManager = $this->getDoctrine()->getManager();
            $adresse="https://tn.tunisiebooking.com/detail_hotel_".$i."/";
            $page1 = file_get_contents ($adresse);
            $page=preg_replace("/\r|\n/","",$page1);
            preg_match_all ('# <h2 class="h2styles" >(.*?)</h2>   <span class="h2styless" style="float:none">#', $page, $name);
            
            for($j = 0; $j < count($name[1]); $j++){
                if (trim($name[1][$j]) <> '') {
                    preg_match_all ('#<p style="text-align: justify;">(.*?)</p>#', $page, $description);
                    preg_match_all ('#<i class="icon-s(.*?)" #', $page, $category);
                    $cat=count($category[1])/2 ."étoil";

                    $providerhotel = new ProviderHotel;

                    $providerhotel->setName($name[1][$j])
                                  ->setLink($adresse)
                                  ->setCategory($cat)
                                  ->AddProvider($provider=$this->getDoctrine()->getRepository(Provider::class)->findOneByName("Tunisiebooking"));
                    if (count($description[1]) == 0) {
                        $providerhotel->setDescription("pas description");
                    }
                    else{
                        $providerhotel->setDescription($description[1][1]);

                    }
                    
                
                    $entityManager->persist($providerhotel);
                    preg_match_all ('#{"id": "0", "src": "(.*?)",#', $page, $img);
                    for($k=0; $k<count($img[1]); $k++){
                        $providerhotelimg = new ProviderHotelImg;
                        $providerhotelimg->setImg($img[1][$k])
                                         ->setProviderhotels($providerhotel);
                        $entityManager->persist($providerhotelimg);
                    }
            
            }

        }
        set_time_limit(1000000);
    }
    $entityManager->flush();
    


        return $this->render('hotel_booking/index.html.twig', [
            'controller_name' => 'HotelBookingController',
        ]);
    }
}
