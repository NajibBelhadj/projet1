<?php

namespace App\Controller;

use App\Entity\Provider;
use App\Entity\ProviderHotel;
use App\Entity\ProviderHotelImg;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HotelController extends AbstractController
{
    /**
     * @Route("/hotel", name="hotel")
     */
    public function index()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $adresse = "https://www.tahavoyages.com/fr/pages/promos";
        $page = file_get_contents ($adresse);
        preg_match_all ('#<a href="(.*?)" class="link">#', $page, $link);
        for($i = 0; $i < count($link[1]); $i++){
            if ($link[1][$i]<>"https://www.tahavoyages.com/fr/Hotel/iberostar-kantaoui-bay"){
                $page = file_get_contents ($link[1][$i]);
                $page2=preg_replace("/\r|\n/","",$page);
                preg_match_all ('#<h1>(.*?)<span>#', $page2, $name);
                preg_match_all ('#div class="descptif">\s*(.*?)\s*</div><br/>\s*<h2>\s*<div class="titre"><span style="font-size: 17px">(.*?)</span></div>\s*</h2>#', $page2, $description);
                preg_match_all ("# <img class='img-responsive lazy' src='../../../themes/TAHA/css/Responsive/images/loading.gif' data-original='(.*?)' alt=''>#", $page2, $image);
                preg_match_all ("#<span class='glyphicon glyphicon-(.*?)'#", $page2, $category);
                $hotel= new ProviderHotel();
                $cat=count($category[1])." étoil";
                if (count($name[1])<>0){
                    $hotel->setName(strtoupper(trim($name[1][0])))
                        ->setCategory($cat)
                        ->setLink($adresse)
                        ->setHotelRef(1)
                        ->AddProvider($provider=$this->getDoctrine()->getRepository(Provider::class)->findOneByName("Tahavoyage"));
                    if (count($description[1]) == 0) {
                        $hotel->setDescription("pas description");
                    }
                    else {
                        for($j = 0; $j < count($description[1]); $j++){
    
                            $hotel->setDescription($description[1][$j]);
                        }
                    }
                    $entityManager->persist($hotel);
                    for($k = 0; $k < count($image[1]); $k++)  {
                        $img = new ProviderHotelImg();
                        $img->setImg($image[1][$k])
                            ->setProviderhotels($hotel);
                        $entityManager->persist($img);
                    }
                }

            }

           
            set_time_limit(1000000);
        }
        $entityManager->flush();
        
        


















        return $this->render('hotel/index.html.twig', [
            'controller_name' => 'HotelController',
        ]);
    }
}
